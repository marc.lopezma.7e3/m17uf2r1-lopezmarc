﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieMachine : StateMachine
{
    [SerializeField]
    public EnemyData enemyData;

    [SerializeField]
    public NavMeshAgent agent;

    [SerializeField]
    public Animator animator;

    [HideInInspector]
    public Patrol patrolState;
    [SerializeField]
    public Transform[] patrollingLocations;

    [HideInInspector]
    public Chase chaseState;
    [HideInInspector]
    public Attack attackState;
    [HideInInspector]
    public Die dieState;

    [HideInInspector]
    public Alert alertState;
    private int distanceToCheck = 3;

    private void Awake()
    {
        patrolState = new Patrol(this);
        chaseState = new Chase(this);
        attackState = new Attack(this);
        dieState = new Die(this);
        alertState = new Alert(this);
    }

    protected override BaseState GetInitialState()
    {
        return patrolState;
    }

    public Vector3 GetRandomPatrolLocation()
    { 
        return patrollingLocations[Random.Range(0, patrollingLocations.Length)].position;
    }

    public Vector3 GetRandomAlertLocation()
    {
        Vector3 position = gameObject.transform.position;
        position.x += Random.Range(-distanceToCheck, distanceToCheck);
        position.y += Random.Range(0, 1);
        position.z += Random.Range(-distanceToCheck, distanceToCheck);
        return position;
    }


    private void OnGUI()
    {
        // string content = currentState != null ? currentState.name : "(no current state)";
        // GUILayout.Label($"<color='black'><size=40>{content}</size></color>");
    }

}