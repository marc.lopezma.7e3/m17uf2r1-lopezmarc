﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Alert : BaseState
{
    private ZombieMachine _zm;

    private bool alert;
    private int count, thoroughnessMultiplier = 3;

    public Alert(ZombieMachine stateMachine) : base("Alert", stateMachine)
    {
        _zm = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();

        _zm.agent.ResetPath();

        alert = false;

        GetMovin();

        Animate();
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();

        if (count == thoroughnessMultiplier)
        {
            _zm.ChangeState(_zm.patrolState);
        }

        if (_zm.agent.velocity.magnitude > 0) _zm.agent.updateRotation = true;

        if (!alert) GetMovin();
        else if (alert && _zm.agent.hasPath == false) _zm.agent.SetDestination(_zm.GetRandomAlertLocation());

        if (ArrivedToDestination() && count < thoroughnessMultiplier) ReAlert();

        if (_zm.gameObject.GetComponent<Enemy>().seeingPlayer) _zm.ChangeState(_zm.chaseState);
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();
    }

    private void GetMovin()
    {
        Vector3 destination = _zm.GetRandomAlertLocation();
        Debug.Log($"Alert to {destination}");
        alert = true;
        _zm.agent.SetDestination(destination);
    }

    private void Animate()
    {
        _zm.animator.SetFloat("Forward", _zm.agent.velocity.magnitude);
    }

    private bool ArrivedToDestination()
    {
        return _zm.agent.remainingDistance < 1f;
    }

    private void ReAlert()
    {
        count++;
        _zm.ChangeState(_zm.alertState);
    }


}
