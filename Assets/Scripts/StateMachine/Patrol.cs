﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//                - Patrol State: L'enemic comença en un estat incial patrulla entre un mínim de 3 punts
//                    - Si player entra al "camp de visió" de l'enemic, aquest el seguiex.

public class Patrol : BaseState
{
    private ZombieMachine _zm;

    private InputAction enterAction;

    private bool patrolling = false;

    public Patrol(ZombieMachine stateMachine) : base("Patrol", stateMachine)
    {
        _zm = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();

        _zm.agent.ResetPath();

        GetMovin();

        Animate();
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();

        if (_zm.agent.velocity.magnitude > 0) _zm.agent.updateRotation = true;

        if (!patrolling) GetMovin();

        if (ArrivedToDestination())  RePatrol();

        if (_zm.gameObject.GetComponent<Enemy>().seeingPlayer) _zm.ChangeState(_zm.chaseState);

        if (enterAction == null) enterAction = GameManager.Instance.Player.GetComponent<PlayerController>().enterAction;
        else if (enterAction.triggered) _zm.ChangeState(_zm.chaseState);
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();
    }

    private void GetMovin()
    {
        patrolling = true;
        Vector3 destination = _zm.GetRandomPatrolLocation();
        Debug.Log($"Patrolling to {destination}");
        _zm.agent.SetDestination(destination);
    }

    private void Animate()
    {
        _zm.animator.SetFloat("Forward", _zm.agent.velocity.magnitude);
    }

    private bool ArrivedToDestination()
    {
        return _zm.agent.remainingDistance < _zm.enemyData.AttackRange;
    }

    private void RePatrol()
    {
        _zm.agent.ResetPath();
        _zm.ChangeState(_zm.patrolState);
    }


}
