﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//                - Persecute State:
//                    - Mentres no estigui suficientment aprop l'enemic seguirà en aquest estat.
//                    - Si la distància es suficcient per atacar, canvia l'estat a Attack.

public class Chase : BaseState
{
    private ZombieMachine _zm;

    private InputAction enterAction;

    public Chase(ZombieMachine stateMachine) : base("Chase", stateMachine)
    {
        _zm = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();

        Go();

        Animate();
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();

        if (_zm.agent.velocity.magnitude > 0) _zm.agent.updateRotation = true;

        if (_zm.gameObject.GetComponent<Enemy>().seeingPlayer && InRange()) _zm.ChangeState(_zm.attackState);

        if (_zm.agent.remainingDistance < _zm.enemyData.AttackRange && !InRange())
        {
            _zm.ChangeState(_zm.alertState);
        }

        if (enterAction == null) enterAction = GameManager.Instance.Player.GetComponent<PlayerController>().enterAction;
        else if (enterAction.triggered) _zm.ChangeState(_zm.patrolState);
        
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();
    }

    private bool InRange()
    {
        float distanceFromPlayer = Vector3.Distance(_zm.gameObject.transform.position, GameManager.Instance.Player.transform.position);
        if (distanceFromPlayer <= _zm.enemyData.AttackRange) return true;
        else return false;
    }

    private void Animate()
    {
        _zm.animator.SetFloat("Forward", _zm.agent.velocity.magnitude);
    }

    private void Go()
    {
        _zm.agent.ResetPath();
        Debug.Log("Chasing Player");
        _zm.agent.SetDestination(GameManager.Instance.PlayerPosition());
    }

}
