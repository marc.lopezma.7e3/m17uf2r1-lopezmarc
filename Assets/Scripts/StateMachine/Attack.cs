﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//                - Attack State:
//                    - Si aconsegueix estar molt aprop del player, l'enemic ataca.
//                    - Si la distància amb el player no és suficient passarà a Persecute State.

public class Attack : BaseState
{
    private ZombieMachine _zm;

    private bool damagedPlayer = false;

    private float timer, attackTime = 0.69f;

    public Attack(ZombieMachine stateMachine) : base("Attack", stateMachine)
    {
        _zm = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();

        timer = 0f;

        if (!damagedPlayer && _zm.gameObject.GetComponent<Enemy>().seeingPlayer && InAttackRange()) DamagePlayer();
        else _zm.ChangeState(_zm.chaseState);
    }

    public override void Exit()
    {
        base.Exit();
        _zm.animator.SetBool("Attack", false);
        damagedPlayer = false;
        timer = 0f;
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();
        Tick();
        if (damagedPlayer) _zm.ChangeState(_zm.chaseState);
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();
    }

    private void Animate()
    {
        _zm.animator.SetBool("Attack", true);
    }

    protected bool CollidedWithPlayer(Collider2D collision)
    {
        if (collision.CompareTag("Player")) return true;
        else return false;
    }

    private void DamagePlayer()
    {
        damagedPlayer = true;
        Animate();
        Debug.Log("Attacking Player");
        GameManager.Instance.DamagePlayer(_zm.enemyData.AttackDamage);
    }

    private bool InAttackRange()
    {
        float distanceFromPlayer = Vector3.Distance(GameManager.Instance.PlayerPosition(), _zm.gameObject.transform.position);
        return distanceFromPlayer < _zm.enemyData.AttackRange;
    }

    private void Tick()
    {
        timer += Time.deltaTime;
        if (timer >= attackTime)
        {
            _zm.ChangeState(_zm.chaseState);
        }
    }
}
