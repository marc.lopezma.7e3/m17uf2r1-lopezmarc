﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Die : BaseState
{
    private ZombieMachine _zm;

    public Die(ZombieMachine stateMachine) : base("Die", stateMachine)
    {
        _zm = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();

        DiePls();
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();

    }

    private void DiePls()
    {
        _zm.agent.ResetPath();

        Animate();
    }

    private void Animate()
    {
        _zm.animator.SetBool("Death", true);
    }
}
