﻿using System;
using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class Enemy : Character, IKillable, IDamageable<float>, IHealable<float>
{
    [SerializeField]
    public ZombieMachine zombieMachine;

    private EnemyData enemyData;

    [SerializeField]
    public SphereCollider visionCollider;
    public bool seeingPlayer = false;

    void Start()
    {
        GameManager.Instance.GetComponent<EnemyManager>().EnemiesInGame.Add(this.gameObject);
        enemyData = gameObject.GetComponent<ZombieMachine>().enemyData;
        visionCollider = gameObject.GetComponentInChildren<SphereCollider>();
        enemyData.ResetStats();
    }

    public void Kill()
    {
        enemyData.Lifes -= 1;
        if (enemyData.Lifes <= 0) zombieMachine.ChangeState(zombieMachine.dieState);
    }

    public void Heal(float amountHealed)
    {
        enemyData.Health += amountHealed;
    }

    public void Damage(float damageTaken)
    {
        enemyData.Health -= damageTaken;
        if (enemyData.Health <= 0)
        {
            Kill();
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider == null) seeingPlayer = false;
        else if (collider.gameObject.tag == "Player") seeingPlayer = true;
        else seeingPlayer = false;

    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player") seeingPlayer = false;
    }

}