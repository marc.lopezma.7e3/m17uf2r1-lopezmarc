﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance { get { return _instance; } }
    private static GameManager _instance;

    [SerializeField]
    public GameObject Player { get { return _player; } }
    private GameObject _player;

    public static EnemyManager EnemyManager;

    public Weapon PlayerWeaponComponent;

    private static GameObject MainCamera;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            Debug.Log("GameManager DESTROYED");
        }
        _instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.FindWithTag("Player");

        EnemyManager = gameObject.GetComponent<EnemyManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_player == null)
        {
            _player = GameObject.FindWithTag("Player");
        }
        if (_player != null && PlayerWeaponComponent == null)
        {
            PlayerWeaponComponent = GameManager.Instance.Player.GetComponentInChildren<Weapon>();
        }
    }

    public void ResetStage()
    {
        ResetPlayerPosition();
        ResetSpawners();
    }

    public void ResetPlayerPosition()
    {
        Player.GetComponent<Player>().ResetPosition();
    }

    public void ResetSpawners()
    {

    }

    public void GameOver()
    {
        StartCoroutine(WaitForGameOverScreen());
    }

    private void ResetGame()
    {
        GoToStartMenu();
    }

    private IEnumerator WaitForGameOverScreen()
    {
        yield return new WaitForSeconds(1);
        ResetGame();
    }

    private void GoToStartMenu()
    {
        SceneManager.LoadScene("RogueLikeStartMenu");
    }

    public void GoToInGameScene()
    {
        SceneManager.LoadScene("RogueLikeInGame");
    }

    public bool SuccessfulRol(float DropChance)
    {
        if (UnityEngine.Random.Range(0, 100) <= DropChance)
        {
            return true;
        }
        else return false;
    }

    public bool AttemptDrop(GameObject[] Drops)
    {
        foreach (GameObject Drop in Drops)
        {
            if (Drop.GetComponent<ItemData>().DropChance <= UnityEngine.Random.Range(0, 100)) return true; else return false;
        }
        return false;
    }

    private void Drop(GameObject drop)
    {
        Instantiate(drop, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 0), Quaternion.identity);
    }

    public void DamagePlayer(float amount)
    {
        // _player.GetComponent<PlayerController>().playerData.Damage(amount);
    }

    public Vector3 PlayerPosition()
    {
        return _player.transform.position;
    }

    public float WeaponDamage()
    {
        return PlayerWeaponComponent.weaponData.Damage;
    }
}
