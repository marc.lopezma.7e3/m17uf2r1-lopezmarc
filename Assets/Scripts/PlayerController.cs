﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Playables;

[RequireComponent(typeof(CharacterController), typeof(PlayerInput))]
public class PlayerController : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private Animator animator;
    [SerializeField] private float playerSpeed = 1.0f;
    [SerializeField] private float runningSpeedMultiplier = 2.0f;
    [SerializeField] private float crouchingSpeedMultiplier = 0.5f;
    [SerializeField] private float jumpHeight = 1.0f;
    [SerializeField] private float gravityValue = -9.81f;
    [SerializeField] private float rotationSpeed = 0.8f;
    [SerializeField] private float jumpForce = 3.0f;

    private PlayerInput playerInput;

    private InputAction moveAction;
    private InputAction jumpAction;
    private InputAction aimAction;
    private InputAction crouchAction;
    private InputAction runAction;
    private InputAction celebrateAction;
    [HideInInspector]
    public InputAction interactAction;
    private InputAction shootAction;
    private InputAction reloadAction;
    [HideInInspector]
    public InputAction switchWeaponAction;
    [HideInInspector]
    public InputAction enterAction;

    private Transform cameraTransform;

    [SerializeField]
    public Weapon weapon;

    private void Awake()
    {

        Cursor.lockState = CursorLockMode.Locked;

        controller = GetComponent<CharacterController>();

        playerInput = GetComponent<PlayerInput>();

        moveAction = playerInput.actions["Move"];
        jumpAction = playerInput.actions["Jump"];
        aimAction = playerInput.actions["Aim"];
        crouchAction = playerInput.actions["Crouch"];
        runAction = playerInput.actions["Run"];
        celebrateAction = playerInput.actions["Celebrate"];
        interactAction = playerInput.actions["Interact"];
        shootAction = playerInput.actions["Shoot"];
        reloadAction = playerInput.actions["Reload"];
        switchWeaponAction = playerInput.actions["SwitchWeapon"];
        enterAction = playerInput.actions["Enter"];

        animator = gameObject.GetComponent<Animator>();
    }

    private void OnEnable()
    {
        shootAction.performed += _ => ShootGun();
    }

    private void OnDisable()
    {
        shootAction.performed -= _ => ShootGun();

    }

    private void ShootGun()
    {
        weapon.Shoot();
        StartCoroutine(AnimateGun());
    }

    private IEnumerator AnimateGun()
    {
        animator.SetLayerWeight(1, 1); // 1 is SHOOTING LAYER
        animator.SetTrigger("Shoot");
        yield return new WaitForSeconds(1);
        animator.ResetTrigger("Shoot");
        animator.SetLayerWeight(1, 0); // 1 is SHOOTING LAYER
    }

    private void UserAskedForResupply()
    {
        if (reloadAction.triggered)
        {
            gameObject.GetComponent<Weapon>().weaponData.Resupply();
            Debug.Log("User asked for Ammo Resupply");
        }
    }

    void Update()
    {
        if (weapon == null) weapon = gameObject.GetComponentInChildren<Weapon>();

        cameraTransform = Camera.main.transform;

        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector2 input = moveAction.ReadValue<Vector2>();
        Vector3 move = new Vector3(input.x, 0, input.y);

        float aimValue = aimAction.ReadValue<float>();
        float crouchValue = crouchAction.ReadValue<float>();
        float runValue = runAction.ReadValue<float>();
        bool jumping = jumpAction.triggered;
        bool celebrating = celebrateAction.triggered;
        bool interacting = interactAction.triggered;

        if (celebrating) AttemptCelebration();

        move = move.x * cameraTransform.right.normalized + move.z * cameraTransform.forward.normalized;
        move.y = 0f;

        // DETERMINE SPEED
        if(runValue == 0 && crouchValue == 0)
        {
            controller.Move(move * Time.deltaTime * playerSpeed);
        }
        else if(runValue > 0 && crouchValue == 0)
        {
            controller.Move(move * Time.deltaTime * playerSpeed * runningSpeedMultiplier);
        }
        else if(crouchValue > 0 && runValue == 0)
        {
            controller.Move(move * Time.deltaTime * playerSpeed * crouchingSpeedMultiplier);
        }
        else if (crouchValue > 0 && runValue > 0)
        {
            controller.Move(move * Time.deltaTime * playerSpeed * crouchingSpeedMultiplier * runningSpeedMultiplier);
        }

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        // Changes the height position of the player..
        if (jumpAction.triggered && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -jumpForce * gravityValue);
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);

        float targetAngle = cameraTransform.eulerAngles.y;
        Quaternion targetRotation = Quaternion.Euler(0, cameraTransform.eulerAngles.y, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);

        Animate(move, aimValue, crouchValue, runValue, jumping, celebrating, interacting);
    }

    private void Animate(Vector3 movement, float aimValue, float crouchValue, float runValue, bool jumping, bool celebrating, bool interacting)
    {
        animator.SetFloat("Forward", movement.magnitude, 0, Time.deltaTime);
        animator.SetBool("OnGround", controller.isGrounded);
        if (controller.isGrounded) // 2 is AIR LAYER
            animator.SetLayerWeight(2, 0);
        else
            animator.SetLayerWeight(2, 1);
        animator.SetFloat("Aiming", aimValue);
        animator.SetLayerWeight(1, aimValue); // 1 is SHOOTING LAYER
        animator.SetFloat("Crouching", crouchValue);
        animator.SetFloat("Running", runValue);
        animator.SetBool("Jumping", jumping);
        animator.SetBool("Celebrating", celebrating);
        animator.SetBool("Interacting", interacting);

    }

    private void OnTriggerStay(Collider collision)
    {
        // IF ITS AN ITEM
        ICollectible collectible = collision.GetComponent<ICollectible>();
        if (collectible != null)
        {
            if (interactAction.triggered) collectible.Collect();
        }
    }

    private void AttemptCelebration()
    {
        if (GameObject.Find("CelebrationPlayableDirector") != null) GameObject.Find("CelebrationPlayableDirector").GetComponent<PlayableDirector>().Play();
    }
}