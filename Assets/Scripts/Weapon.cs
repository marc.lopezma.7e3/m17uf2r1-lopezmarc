﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public WeaponScriptableObject weaponData;
    public Transform firePoint;
    public Rigidbody rb;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();

        if (weaponData != null)
        {
            weaponData.ResetWeapon();
            // Change MESH / MODEL / SMTH?
        }
    }

    public void Shoot()
    {
        Transform cameraTransform = Camera.main.transform;
        if (weaponData.CanShoot())
        {
            RaycastHit hit;
            GameObject bullet = GameObject.Instantiate(weaponData.BulletType, firePoint.position, Quaternion.identity);
            BulletController bulletController = bullet.GetComponent<BulletController>();
            if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, Mathf.Infinity))
            {
                bulletController.target = hit.point;
                bulletController.hit = true;
            }
            else
            {
                bulletController.target = cameraTransform.position + cameraTransform.forward * weaponData.HitMissDistance;
                bulletController.hit = false;
            }
        }
    }


}
