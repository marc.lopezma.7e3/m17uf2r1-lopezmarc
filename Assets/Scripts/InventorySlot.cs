﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{

    // public Image image;
    public Text nameText;
    // public Text stackSizeText;

    public void ClearSlot()
    {
        Debug.Log("Disabling Slot Components");
        // image.enabled = false;
        nameText.enabled = false;
        // stackSizeText.enabled = false;
    }

    public void DrawSlot(InventoryItem item)
    {
        if (item.itemData == null)
        {
            Debug.Log($"{item.itemData.Name} was null");
            ClearSlot();
            return;
        }

        Debug.Log("Enabling Slot Components");
        // image.enabled = true;
        nameText.enabled = true;
        // stackSizeText.enabled = true;

        //icon.Mesh = item.itemData.Icon;
        nameText.text = item.itemData.Name;
        // stackSizeText.text = item.stackSize.ToString();
    }
}