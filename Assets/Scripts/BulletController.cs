﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{

    [SerializeField]
    private GameObject bulletDecal;

    private float speed = 50f;
    private float timeToDestroy = 3f;

    public Vector3 target { get; set; }
    public bool hit { get; set; }

    public SphereCollider bulletCollider;
    public Rigidbody bulletBody;

    // Start is called before the first frame update
    void Start()
    {
        bulletCollider = gameObject.GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if(!hit & Vector3.Distance(transform.position, target) < .01f)
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        Destroy(gameObject, timeToDestroy);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Bullet Collided");
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("HitEnemy");
            collision.gameObject.GetComponent<Enemy>().Damage(GameManager.Instance.WeaponDamage());

            Destroy(gameObject);
        }
        if (!collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Hit anything but player");
            ContactPoint contact = collision.GetContact(0);
            GameObject.Instantiate(bulletDecal, contact.point + contact.normal * 0.0001f, Quaternion.LookRotation(contact.normal));
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Bullet Collided");
        if (collider.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("HitEnemy");
            collider.gameObject.GetComponentInParent<Enemy>().Damage(GameManager.Instance.WeaponDamage());

            Destroy(gameObject);
        }
    }
}
