﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemData : ScriptableObject
{
    [Header("Name")]
    public string Name;

    [Header("Type")]
    public ItemType Type;
    public enum ItemType
    {
        Gun,
        Buff,
        Consumable
    }

    [Header("Item Stats")]
    public int DropChance;
    public int Price;

    [Header("Model")]
    public Mesh Icon;

}
